const searchParams = new URLSearchParams(location.search);
const missionId = searchParams.get('id');

/**
 * Handle log out
 */
document.querySelector(".logout").addEventListener("click", async function (event) {

    const response = await fetch("/logout");

    const result = await response.json();

    if (response.status === 200) {
        window.location = '/';
    }
    else {
        console.log(result);
    }

});

async function loadMissionDetail() {

    loadProfileButtonAndBalacne();

    const response1 = await fetch(`/mission/${missionId}`);
    const missionDetail = await response1.json();

    const response6 = await fetch(`/current-user`);
    const currentUser = await response6.json();

    const response5 = await fetch(`/is-join/${missionId}/${currentUser.id}`);
    const isJoin = await response5.json();

    const response2 = await fetch(`/num-of-patrons/${missionId}`);
    const numOfPatrons = await response2.json();

    const response3 = await fetch(`/list-of-patrons/${missionId}`);
    const listOfPatrons = await response3.json();

    let nameList = "";
    for (const patron of listOfPatrons)
        nameList += `${patron.firstname} ${patron.lastname}&nbsp&nbsp&nbsp&nbsp`;

    // Reder the page below
    const missionContainer = document.querySelector(".mission-container");
    missionContainer.innerHTML = "";

    let joinBtn = "";

    if (isJoin.length > 0) {
        joinBtn = `<div class="join-btn d-none">參加</div><div class="isjoined-btn">已參加</div>`
    }
    else {
        joinBtn = `<div class="join-btn">參加</div><div class="isjoined-btn d-none">已參加</div>`
    }

    missionContainer.innerHTML =
        `<div class="row">
            <div class="col">
                <div class="image-frame">
                    <img src="./uploads/${missionDetail.image}" alt="">
                </div>
            </div>
            <div class="col">
                <h4 class="mission-title">${missionDetail.title}</h4>
                <p class="mission-content">${missionDetail.content}</p>
                <i class="mission-lastupdated">這項任務由 ${missionDetail.firstname} 提供 任務最後更新 ${convertTime(missionDetail.updated_at)}</i>
                <p class="mission-dtime">時間: ${convertTime(missionDetail.delivery_time)}</p>
                <p>地點: ${missionDetail.location}</p>
                <div class="ask-for">${missionDetail.serving}:&nbsp&nbsp
                    <div class="m-coin-sign">
                        <div class="m-sign">M</div>
                        <div class="double-line">| |</div>
                    </div>
                    &nbsp${missionDetail.coin_demand}&nbsp作報酬
                </div>
                <p>尚餘名額: ${missionDetail.max_patrons - numOfPatrons.count} 個 (總數: ${missionDetail.max_patrons} 個)</p>
                <p>截止時間: 尚餘 ${timeLeftInMinutes(missionDetail.expiration)} 分鐘</p>

                ${joinBtn}
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="namelist-label">這項任務有以下的參加者</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <i class="patron-namelist">${nameList}</i>
            </div>
        </div>`


    /////////////////////// Added by Leung on 7:41pm 18-Jul-20 /////////////////////////////////
    if (numOfPatrons.count == missionDetail.max_patrons) {
        document.querySelector(".join-btn").classList.add('d-none');
    }
    ////////////////////// Added by Leung on 7:41pm 18-Jul-20 //////////////////////////////////


    document.querySelector(".join-btn").addEventListener('click', async () => {


        /////////////////////// Added by Leung on 6:24pm 18-Jul-20 /////////////////////////////
        const response_user = await fetch(`/current-user`);
        const current_user = await response_user.json();

        const response_coins_outstanding = await fetch(`/coins-outstanding/${current_user.id}`);
        const coins_outstanding = await response_coins_outstanding.json();

        if (current_user.num_coin < (parseInt(missionDetail.coin_demand) + parseInt(coins_outstanding.sum))) {
            alert(`你只有 ${current_user.num_coin - coins_outstanding.sum} 個可用餘額。`);
            return;
        }
        /////////////////////////// Added by Leung on 6:24pm 18-Jul-20 //////////////////////////

        const response = await fetch(`/question/${missionId}`);

        const result = await response.json();

        const reply = prompt(`${result.question}`);

        if (reply) {
            await fetch(`/patron`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    mission_id: missionId,
                    answer: reply
                })
            })
        }
        else {
            alert("請回答對方給你的問題!")
        }

        // We don't have to reload
        loadMissionDetail();   //////////////// Added by Leung on 8:18pm 18-Jul-20 ////////////////////////////
    });
}

async function loadProfileButtonAndBalacne() {

    const response1 = await fetch('/current-user');
    const user = await response1.json();

    const response2 = await fetch(`/coins-outstanding/${user.id}`);
    const coinNeeded = await response2.json();

    let profileIcon = document.querySelector(".profile-icon")

    if (user.picture) {
        profileIcon.style.backgroundImage = `url("${user.picture}")`;
        profileIcon.style.backgroundSize = "40px 40px";
    }
    else {

        const userIntial = user.firstname[0] + user.lastname[0];
        profileIcon.innerHTML = userIntial.toUpperCase();
    }

    document.querySelector(".actual-amount").innerHTML = user.num_coin;
    document.querySelector(".avaliable-amount").innerHTML = user.num_coin - coinNeeded.sum;
}

loadMissionDetail();

