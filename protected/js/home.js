// for 建立任務 form image preview
document.querySelector("#image-file").addEventListener("change", () => {

    const imageFrame = document.querySelector(".image-frame");

    imageFrame.innerHTML = "";

    if (event.target.files.length > 0) {

        let objectURL = URL.createObjectURL(event.target.files[0]);

        let image = document.createElement('img');
        image.src = objectURL;

        let missionImageWrapper = document.createElement('div');
        missionImageWrapper.classList.add("mission-image")
        missionImageWrapper.append(image);

        imageFrame.append(missionImageWrapper);
    }
});

/**
 * Handle create new mission
 */
document.querySelector(".commit-btn").addEventListener("click", async function (event) {
    event.preventDefault();

    const missionForm = document.querySelector("#mission-form");

    // Check the form here. If the form is not valid, stop propagate, which means
    // stop submitting the form, display the invalid feedback.
    if (missionForm.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        missionForm.classList.add('was-validated');
        return;
    }

    const missionData = new FormData();

    if (missionForm.image.files[0])
        missionData.append('image', missionForm.image.files[0]);

    const category = missionForm.elements.category.value;
    missionData.append('category', category);
    const title = missionForm.elements.title.value;
    missionData.append('title', title);
    const content = missionForm.elements.content.value;
    missionData.append('content', content);
    const maxPatrons = missionForm.elements.maxPatrons.value;
    missionData.append('maxPatrons', maxPatrons);
    const coinDemand = missionForm.elements.coinDemand.value;
    missionData.append('coinDemand', coinDemand);
    const serving = missionForm.elements.serving.value;
    missionData.append('serving', serving);
    const deliveryPlace = missionForm.elements.deliveryPlace.value;
    missionData.append('deliveryPlace', deliveryPlace);
    const deliveryTime = missionForm.elements.deliveryTime.value;
    missionData.append('deliveryTime', deliveryTime);
    const expiration = missionForm.elements.expiration.value;
    missionData.append('expiration', expiration);
    const question = missionForm.elements.question.value;
    missionData.append('question', question);

    const response = await fetch("/mission", {
        method: "POST",
        body: missionData
    });

    // Added by Leung on 5:04pm 15-Jul-20
    // if (response.status === 401) {
    //     alert("Please login first.");
    //     return;
    // }
    // Added by Leung on 5:04pm 15-Jul-20

    const result = await response.json();

    ///////////////////// Added by Leung on 7:28 20-Jul-20 to reset the form after submit /////////////////////////
    missionForm.reset();
    const imageFrame = document.querySelector(".image-frame");
    imageFrame.innerHTML = "";
    ///////////////////// Added by Leung on 7:28 20-Jul-20 /////////////////////////

    loadMissions();
});

/**
 * Handle log out
 */
document.querySelector(".logout").addEventListener("click", async function (event) {

    const response = await fetch("/logout");

    const result = await response.json();

    if (response.status === 200) {
        window.location = '/';
    }
    else {
        console.log(result);
    }

});

async function loadMissions() {

    loadProfileButtonAndBalacne()
    loadLocations();

    const response1 = await fetch("/missions");
    const missions = await response1.json();

    const response3 = await fetch("/current-user");
    const currentUser = await response3.json();

    let cards = document.querySelector(".mission-cards");
    cards.innerHTML = "";

    for (let mission of missions) {

        if (currentUser.id != mission.contributor_id && timeLeftInMinutes(mission.expiration) > 0) {
            const response2 = await fetch(`/num-of-patrons/${mission.mission_id}`);
            const numOfPatrons = await response2.json();

            cards.innerHTML +=
                `<a href="/mission.html?id=${mission.mission_id}" target="_self">
                <div class="mission-card">
                    <div class="card-image">
                        <img src="./uploads/${mission.image}">
                    </div>
                    <div class="card-body">
                        <p class="title">${mission.title}</p>
                        <p class="descr">${mission.content}</p>
                        <p>交收日期:&nbsp${convertTime(mission.delivery_time)}</p>
                        <p>交收地點:&nbsp${mission.location}</p>
                    </div>
                    <div class="icons">
                        <div class="coin-need">
                            <div class="amount">
                                <div class="m-dollar-sign">
                                    <div class="m-sign">M</div>
                                    <div class="double-line">| |</div>
                                </div>
                                <div>${mission.coin_demand}</div>
                            </div>
                            <div class="unit">${mission.serving}</div>
                        </div>
                        <div class="vacancy">
                            <div>${mission.max_patrons - numOfPatrons.count}/${mission.max_patrons}</div>
                            <div class="unit">尚餘名額</div>
                        </div>
                        <div class="time-left">
                            <div class='mins'>${timeLeftInMinutes(mission.expiration)}</div>
                            <div class="unit">尚餘分鐘</div>
                        </div>
                    </div>
                </div>
            </a>`
        }
    }

    cards.innerHTML += `<div class="empty-card"></div><div class="empty-card"></div><div class="empty-card"></div>`
}

document.querySelector(".join-record").addEventListener('click', async () => {

    event.currentTarget.classList.add("active");
    document.querySelector(".create-record").classList.remove("active");

    const response1 = await fetch('/join-histories');
    const joinHistories = await response1.json();

    let cards = document.querySelector(".mission-cards");
    cards.innerHTML = "";


    if (joinHistories.length == 0) {
        cards.innerHTML = `<h2 class="no-record">你仍未有任何參加紀錄</h2>`;

    } else {
        for (const history of joinHistories) {

            const response3 = await fetch(`/num-of-patrons/${history.mission_id}`);
            const numOfPatrons = await response3.json();

            const response4 = await fetch(`/location/${history.delivery_place_id}`);
            const location = await response4.json();

            cards.innerHTML +=
                `<a href="/join_history.html?event_id=${history.event_id}" target="_self">
                    <div class="mission-card">
                        <div class="card-image">
                            <img src="./uploads/${history.image}">
                        </div>
                        <div class="card-body">
                            <p class="title">${history.title}</p>
                            <p class="descr">${history.content}</p>
                            <p>交收日期:&nbsp${convertTime(history.delivery_time)}</p>
                            <p>交收地點:&nbsp${location.location}</p>
                        </div>
                        <div class="icons">
                            <div class="coin-need">
                                <div class="amount">
                                    <div class="m-dollar-sign">
                                        <div class="m-sign">M</div>
                                        <div class="double-line">| |</div>
                                    </div>
                                    <div>${history.coin_demand}</div>
                                </div>
                                <div class="unit">${history.serving}</div>
                            </div>
                            <div class="vacancy">
                                <div>${history.max_patrons - numOfPatrons.count}/${history.max_patrons}</div>
                                <div class="unit">尚餘名額</div>
                            </div>
                            <div class="time-left">
                                <div class='mins'>${timeLeftInMinutes(history.expiration) > 0 ? timeLeftInMinutes(history.expiration) : "巳過期"}</div>
                                <div class="unit">尚餘分鐘</div>
                            </div>
                        </div>
                    </div>
                </a>`
        }

        cards.innerHTML += `<div class="empty-card"></div><div class="empty-card"></div><div class="empty-card"></div>`

    }
})


document.querySelector(".create-record").addEventListener('click', async () => {

    event.currentTarget.classList.add("active");
    document.querySelector(".join-record").classList.remove("active");

    const response1 = await fetch('/contribute-histories');
    const contributeHistories = await response1.json();

    let cards = document.querySelector(".mission-cards");
    cards.innerHTML = "";

    if (contributeHistories.length == 0) {

        cards.innerHTML = `<h2 class="no-record">你仍未有任何任務紀錄</h2>`;
    }
    else {

        for (const history of contributeHistories) {

            // Added by Leung on 2:22pm 19-Jul-20
            const response_num = await fetch(`/num-of-patrons/${history.id}`);
            const numOfPatrons = await response_num.json();
            // Added by Leung on 2:22pm 19-Jul-20
            // In line 87, also change from 尚餘 3 個名額 to 尚餘 ${history.max_patrons - numOfPatrons.count} 個名額


            cards.innerHTML +=
                `<a href="/contribute_history.html?id=${history.id}" target="_self">
                    <div class="mission-card">
                        <div class="card-image">
                            <img src="./uploads/${history.image}">
                        </div>
                        <div class="card-body">
                            <p class="title">${history.title}</p>
                            <p class="descr">${history.content}</p>
                            <p>交收日期:&nbsp${convertTime(history.delivery_time)}</p>
                            <p>交收地點:&nbsp${history.location}</p>
                        </div>
                        <div class="icons">
                            <div class="coin-need">
                                <div class="amount">
                                    <div class="m-dollar-sign">
                                        <div class="m-sign">M</div>
                                        <div class="double-line">| |</div>
                                    </div>
                                    <div>${history.coin_demand}</div>
                                </div>
                                <div class="unit">${history.serving}</div>
                            </div>
                            <div class="vacancy">
                                <div>${history.max_patrons - numOfPatrons.count}/${history.max_patrons}</div>
                                <div class="unit">尚餘名額</div>
                            </div>
                            <div class="time-left">
                                <div class='mins'>${timeLeftInMinutes(history.expiration) > 0 ? timeLeftInMinutes(history.expiration) : "巳過期"}</div>
                                <div class="unit">尚餘分鐘</div>
                            </div>
                        </div>
                    </div>
                </a>`;
        }

        cards.innerHTML += `<div class="empty-card"></div><div class="empty-card"></div><div class="empty-card"></div>`;
    }

})

async function loadProfileButtonAndBalacne() {

    const response1 = await fetch('/current-user');
    const user = await response1.json();

    const response2 = await fetch(`/coins-outstanding/${user.id}`);
    const coinNeeded = await response2.json();

    let profileIcon = document.querySelector(".profile-icon")

    if (user.picture) {
        profileIcon.style.backgroundImage = `url("${user.picture}")`;
        profileIcon.style.backgroundSize = "40px 40px";
    }
    else {

        const userIntial = user.firstname[0] + user.lastname[0];
        profileIcon.innerHTML = userIntial.toUpperCase();
    }

    document.querySelector(".actual-amount").innerHTML = user.num_coin;
    document.querySelector(".avaliable-amount").innerHTML = user.num_coin - coinNeeded.sum;
}

document.body.addEventListener("scroll", () => {

    let navBar = document.querySelector(".nav-bar");

    if (document.body.scrollTop >= 380) {

        navBar.style.backgroundColor = "white";
        navBar.style.boxShadow = "0px 2px 5px 0px rgba(0, 0, 0, 0.2)"
    }
    else {
        navBar.style.backgroundColor = "rgba(255, 255, 255, 0.3)";
        navBar.style.borderBottom = "none";
    }
})


/**
 * Load a list of locations from database to the delivery_place combox
 */
async function loadLocations() {

    const response = await fetch('/locations');
    const locations = await response.json();

    let locationList = document.querySelector('#delivery-place-list');

    locationList.innerHTML = "";

    for (const location of locations)
        locationList.innerHTML += `<option value="${location.location}"></option>`
}

/**
 * Set the deliveryTime & expiration in 建立任務 has a min value of now
 */
document.querySelector(".new-mission").addEventListener("click", () => {

    const now = new Date();
    const localeString = now.toLocaleString();

    const nowString = `${localeString.substr(6, 4)}-${localeString.substr(3, 2)}-${localeString.substr(0, 2)}T${localeString.substr(12, 5)}`

    document.querySelector("#deliveryTime").min = nowString;
    document.querySelector("#expiration").min = nowString;
})

document.querySelector("#expiration").addEventListener("change", () => {

    const expiration = event.currentTarget.value;
    const deliveryTime = document.querySelector("#deliveryTime").value;

    if (deliveryTime && (expiration > deliveryTime)) {

        event.currentTarget.value = "";
        alert("截止日期必需在交收日期之前！");
    }
})

document.querySelector("#deliveryTime").addEventListener("change", () => {

    const deliveryTime = event.currentTarget.value;
    const expiration = document.querySelector("#expiration").value;

    if (expiration && (expiration > deliveryTime)) {

        event.currentTarget.value = "";
        alert("截止日期必需在交收日期之前！");
    }
})

// document.querySelector("#nav-icon-wrapper").addEventListener("click", () => {

//     document.querySelector(".nav-icon").classList.toggle("shut");
// })

loadMissions();



