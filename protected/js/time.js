const LOCALTIMEZONE = 8 * 60 * 60 * 1000;

function convertTime(timeString, zone = 8 * 60 * 60 * 1000) {

    const time = new Date(timeString.slice(0, -5));
    const timeValue = time.valueOf() + zone
    const date = new Date(timeValue);


    var months = { 'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12' }

    let _time = date.toString().substr(16, 5);
    let month = date.toString().substr(4, 3);
    let day = date.toString().substr(8, 2);
    let year = date.toString().substr(11, 4);

    return year + "-" + months[month] + "-" + day + " " + _time;
}

function timeLeftInMinutes(timeString) {

    const date = new Date(convertTime(timeString));
    const difference = date.valueOf() - Date.now();
    const differneceInMintues = Math.floor(difference / (1000 * 60));

    return differneceInMintues;
}