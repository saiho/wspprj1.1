/**
 * Handle log out
 */
document.querySelector(".logout").addEventListener("click", async function (event) {

    const response = await fetch("/logout");

    const result = await response.json();

    if (response.status === 200) {
        window.location = '/';
    }
    else {
        console.log(result);
    }

});

document.querySelector(".balance-btn").addEventListener("click", () => {

    event.currentTarget.classList.add("active");
    document.querySelector(".message-btn").classList.remove("active")


    loadBalance();
})

document.querySelector(".message-btn").addEventListener("click", () => {

    event.currentTarget.classList.add("active");
    document.querySelector(".balance-btn").classList.remove("active")

    loadMessages();
})


async function loadBalance() {

    const response = await fetch(`/current-user`);
    const mySelf = await response.json();

    const response1 = await fetch('/income-histories');
    const incomes = await response1.json();

    let container = document.querySelector('.wrapper');

    container.innerHTML = "";
    container.innerHTML =
        `<div class="row titles">
            <div class="col-3">
                建立日期
            </div>
            <div class="col-4">
                任務標題
            </div>
            <div class="col-3">
                參加者
            </div>
            <div class="col-1">
                巳收
            </div>
            <div class="col-1">
                未收
            </div>
        </div>`;

    let collected = 0;
    let uncollected = 0;

    for (const income of incomes) {

        const response = await fetch(`/user-name/${income.patron_id}`);
        const joiner = await response.json();

        if (income.is_completed)
            collected += income.coin_demand;
        else
            uncollected += income.coin_demand;

        container.innerHTML +=
            `<div class="row">
                <div class="col-3">
                    ${convertTime(income.created_at)}
                </div>
                <div class="col-4">
                    <a href="/contribute_history.html?id=${income.mission_id}" target="_self" class="title-link">${income.title}</a>
                </div>
                <div class="col-3">
                    ${joiner.firstname} ${joiner.lastname}
                    <i class="far fa-comment send-patron" data-mission-id="${income.mission_id}" data-patron-id="${income.patron_id}"></i>
                </div>
                <div class="col-1">
                    ${income.is_completed ? income.coin_demand : ""}
                </div>
                <div class="col-1">
                    ${income.is_completed ? "" : income.coin_demand}
                </div>
            </div>`;
    }

    document.querySelector(".collected-amount").innerHTML = collected;
    document.querySelector(".uncollected-amount").innerHTML = uncollected;

    const response2 = await fetch('/join-histories');
    const payments = await response2.json();


    let paid = 0;
    let unpaid = 0;

    container.innerHTML +=
        `<div class="row titles">
            <div class="col-3">
                建立日期
            </div>
            <div class="col-4">
                任務標題
            </div>
            <div class="col-3">
                貢獻者
            </div>
            <div class="col-1">
                已付
            </div>
            <div class="col-1">
                未付
            </div>
        </div>`;


    for (const payment of payments) {

        const response = await fetch(`/user-name/${payment.contributor_id}`);
        const contributor = await response.json();

        if (payment.is_completed)
            paid += payment.coin_demand;
        else
            unpaid += payment.coin_demand;

        container.innerHTML +=
            `<div class="row">
                <div class="col-3">
                    ${convertTime(payment.created_at)}
                </div>
                <div class="col-4">
                    <a href="/join_history.html?event_id=${payment.event_id}" target="_self" class="title-link">${payment.title}</a>
                </div>
                <div class="col-3">
                    ${contributor.firstname} ${contributor.lastname}
                    <i class="far fa-comment send-contributor" data-mission-id="${payment.mission_id}" data-patron-id="${mySelf.id}"></i>
                </div>
                <div class="col-1">
                    ${payment.is_completed ? payment.coin_demand : ""}
                </div>
                <div class="col-1">
                    ${payment.is_completed ? "" : payment.coin_demand}
                </div>
            </div>`;
    }

    document.querySelector(".paid-amount").innerHTML = paid;
    document.querySelector(".unpaid-amount").innerHTML = unpaid;

    let sendPatrons = document.querySelectorAll(".send-patron");

    for (const sendPatron of sendPatrons) {

        sendPatron.addEventListener('click', async () => {

            const missionId = event.currentTarget.dataset.missionId;
            const patronId = event.currentTarget.dataset.patronId;

            const message = prompt("請寫上你要給對方的訊息。");

            if (message) {
                await fetch('/send-patron', {

                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        missionId: missionId,
                        patronId: patronId,
                        message: message
                    })
                })
            }
        })
    }

    let sendContributors = document.querySelectorAll(".send-contributor");

    for (const sendContributor of sendContributors) {

        sendContributor.addEventListener("click", async () => {

            const missionId = event.currentTarget.dataset.missionId;
            const patronId = event.currentTarget.dataset.patronId;

            const message = prompt("請寫上你要給對方的訊息。");

            if (message) {
                await fetch('/send-contributor', {

                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        missionId: missionId,
                        patronId: patronId,
                        message: message
                    })
                })
            }
        })

    }
}

async function loadMessages() {

    const response1 = await fetch(`/current-user`);
    const mySelf = await response1.json();

    const response2 = await fetch(`/list-of-missions/${mySelf.id}`);
    const listOfMyMissions = await response2.json();

    let container = document.querySelector('.wrapper');
    container.innerHTML = "";

    for (const mission of listOfMyMissions) {
        // loop over the missions that i've created

        container.innerHTML +=
            `<div class="row">
                <div class="col titles">
                    ${mission.title} 由 我 於 ${convertTime(mission.created_at)} 建立
                </div>
            </div>`;

        // get a list of patrons with an specified missions.id
        const response3 = await fetch(`/list-of-patrons/${mission.mission_id}`);
        const listOfPatrons = await response3.json();

        for (const patron of listOfPatrons) {

            const response4 = await fetch(`/conversations/${mission.mission_id}/${patron.joiner_id}`);
            const conversations = await response4.json();

            for (const conversation of conversations) {

                container.innerHTML +=
                    `<div class="row">
                        <div class="col message">
                            ${convertTime(conversation.created_at)} ${conversation.is_patron_msg ? conversation.firstname : "我"}: ${conversation.message}
                        </div>
                    </div>`

            }
        }
    }

    const response5 = await fetch(`/join-histories`);
    const listOfJoined = await response5.json();
    console.log(listOfJoined);
    console.log(listOfJoined.length);

    for (const joined of listOfJoined) {

        const response6 = await fetch(`/user-name/${joined.contributor_id}`)
        const contributor = await response6.json();

        container.innerHTML +=
            `<div class="row">
                <div class="col titles">
                    ${joined.title} 由 ${contributor.firstname} ${contributor.lastname} 於 ${convertTime(joined.created_at)} 建立
                </div>
            </div>`;

        const response7 = await fetch(`/conversations/${joined.mission_id}/${mySelf.id}`);
        const conversations = await response7.json();

        for (const conversation of conversations) {

            container.innerHTML +=
                `<div class="row">
                    <div class="col message">
                        ${convertTime(conversation.created_at)} ${conversation.is_patron_msg ? "我" : conversation.firstname}: ${conversation.message}
                    </div>
                </div>`
        }

    }
}

async function loadProfileButtonAndBalacne() {

    const response1 = await fetch('/current-user');
    const user = await response1.json();

    const response2 = await fetch(`/coins-outstanding/${user.id}`);
    const coinNeeded = await response2.json();

    let profileIcon = document.querySelector(".profile-icon");
    let profilePicture = document.querySelector(".profile-pic-lg");

    if (user.picture) {

        profileIcon.style.backgroundImage = `url("${user.picture}")`;
        profileIcon.style.backgroundSize = "40px 40px";

        profilePicture.style.backgroundImage = `url("${user.picture}")`;
        profilePicture.style.backgroundSize = "150px 150px";
    }
    else {

        const userIntial = user.firstname[0] + user.lastname[0];
        profileIcon.innerHTML = userIntial.toUpperCase();
        profilePicture.innerHTML = userIntial.toUpperCase();
    }

    document.querySelector(".name").innerHTML = user.firstname;
    document.querySelector(".actual-amount").innerHTML = user.num_coin;
    document.querySelector(".avaliable-amount").innerHTML = user.num_coin - coinNeeded.sum;
}

loadProfileButtonAndBalacne()