/**
 * Handle log in
 */
document.querySelector(".login-btn").addEventListener("click", async function (event) {
    event.preventDefault();

    const loginForm = document.querySelector("#login-form");

    // Check the form here. If the form is not valid, stop propagate, which means
    // stop submitting the form, display the invalid feedback.
    if (loginForm.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        loginForm.classList.add('was-validated');
        return;
    }

    const response = await fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            email: loginForm.elements.email.value,
            password: loginForm.elements.password.value
        })
    });

    const result = await response.json();

    if (response.status === 200 && result.success) {
        window.location = '/home.html';
    }
    else {
        //// Added by Leung on 1:49 20-Jul-20
        alert("電郵地址或密碼不符！新用戶請先登記。");
    }
});

/**
 * Handle sign up
 */
document.querySelector(".signup-btn").addEventListener("click", async function (event) {
    event.preventDefault();

    const signupForm = document.querySelector("#signup-form");

    // Check the form here. If the form is not valid, stop propagate, which means
    // stop submitting the form, display the invalid feedback.
    if (signupForm.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        signupForm.classList.add('was-validated');
        return;
    }

    const response = await fetch('/signup', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            firstname: signupForm.elements.firstname.value,
            lastname: signupForm.elements.lastname.value,
            mobile: signupForm.elements.mobile.value,
            email: signupForm.elements.email.value,
            password: signupForm.elements.password.value,
        })
    });

    const result = await response.json();

    signupForm.reset();
});

async function loadMissions() {

    let cards = document.querySelector(".mission-cards");
    cards.innerHTML = "";

    const response1 = await fetch("/missions");
    const missions = await response1.json();

    for (let mission of missions) {

        if (timeLeftInMinutes(mission.expiration) > 0) {

            const response2 = await fetch(`/num-of-patrons/${mission.mission_id}`);
            const numOfPatrons = await response2.json();

            cards.innerHTML +=
                `<div class="mission-card">
                <div class="card-image">
                    <img src="./uploads/${mission.image}">
                </div>
                <div class="card-body">
                    <p class="title">${mission.title}</p>
                    <p class="descr">${mission.content}</p>
                    <p>交收日期:&nbsp${convertTime(mission.delivery_time)}</p>
                    <p>交收地點:&nbsp${mission.location}</p>
                </div>
                <div class="icons">
                    <div class="coin-need">
                        <div class="amount">
                            <div class="m-dollar-sign">
                                <div class="m-sign">M</div>
                                <div class="double-line">| |</div>
                            </div>
                            <div>${mission.coin_demand}</div>
                        </div>
                        <div class="unit">${mission.serving}</div>
                    </div>
                    <div class="vacancy">
                        <div>${mission.max_patrons - numOfPatrons.count}/${mission.max_patrons}</div>
                        <div class="unit">尚餘名額</div>
                    </div>
                    <div class="time-left">
                        <div class='mins'>${timeLeftInMinutes(mission.expiration)}</div>
                        <div class="unit">尚餘分鐘</div>
                    </div>
                </div>
            </div>`
        }
    }

    document.querySelector('.mission-cards').addEventListener('click', () => {
        alert("請先登記或登入。");
    })

    cards.innerHTML += `<div class="empty-card"></div><div class="empty-card"></div><div class="empty-card"></div>`
}

//// Added by Leung on 21-Jul-20
async function checkLogin() {

    const res = await fetch('/current-user');
    const user = await res.json();
    if (res.status === 200 && user) {
        window.location = '/home.html';
    }
}

document.body.addEventListener("scroll", () => {

    let navBar = document.querySelector(".nav-bar");

    if (document.body.scrollTop >= 380) {
        navBar.style.backgroundColor = "white";
        // navBar.style.borderBottom = "1px solid #AAAAAA";
        navBar.style.boxShadow = "0px 2px 5px 0px rgba(0, 0, 0, 0.2)"

    }
    else {
        navBar.style.backgroundColor = "rgba(255, 255, 255, 0.3)";
        navBar.style.borderBottom = "none";
    }
})

checkLogin();

loadMissions();
